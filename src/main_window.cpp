#include "main_window.h"
#include "ui_main_window.h"
#include "temp.h"

#include <QColorDialog>
#include <QFileDialog>
#include <QtGlobal>

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Main_Window),
    img_ops_(&data_),
    analyzer_(&data_)
{
    ui_->setupUi(this);
    this->setWindowTitle("GT Creator");
    ui_->image_view->setData(&data_);

    ui_->groupbox_sets->setEnabled(false);
    ui_->groupbox_visualization->setEnabled(false);
    ui_->groupbox_corner_detection->setEnabled(false);

    ui_->button_load_sets->setEnabled(false);
    ui_->button_save_corners->setEnabled(false);

    ui_->checkbox_show_corners->setEnabled(false);
    ui_->button_generate_normalized_images->setEnabled(false);

    ui_->selector_image_version->setCurrentIndex(1);

    ui_->frame_chart->setStyleSheet("background-color:white;");


    thread_img_ops_ = new QThread();
    img_ops_.moveToThread(thread_img_ops_);
    thread_data_ = new QThread();
    data_.moveToThread(thread_data_);
    thread_analyzer_ = new QThread();
    analyzer_.moveToThread(thread_analyzer_);

    qRegisterMetaType<Analysis>("Analysis");

    //connect lists
    connect(ui_->list_sets, &QListWidget::itemSelectionChanged, this, &Main_Window::listItemClicked);

    //connect IO
    connect(ui_->button_load_sets, &QPushButton::clicked, this, &Main_Window::loadImageSets);
    connect(ui_->button_load_MLA, &QPushButton::clicked, this, &Main_Window::loadMLAConfig);
    connect(ui_->button_save_corners, &QPushButton::clicked, &data_, &Data::saveDetectedCorners);

    //connect corner detection elements
    connect(ui_->selector_corner_method, qOverload<int>(&QComboBox::activated), [this](int method){data_.gt_detection_method = method;});
    connect(ui_->button_generate_normalized_images, &QPushButton::clicked, &img_ops_, &Image_Ops::generateNormalizedImages);
    connect(ui_->button_calculate_corners_single, &QPushButton::clicked, [this](){setThresholds(); emit calculateCorners(data_.current_set_index);});
    connect(ui_->button_calculate_corners, &QPushButton::clicked, [this](){setThresholds(); emit calculateAllCorners();});

    //connect visualization elements
    connect(ui_->selector_image_type, qOverload<int>(&QComboBox::activated), [this](int image_type){data_.current_image_type = image_type*3; listItemClicked();});
    connect(ui_->selector_image_version, qOverload<int>(&QComboBox::activated), [this](){listItemClicked();});
    connect(ui_->selector_visu_corners, qOverload<int>(&QComboBox::activated), [this](int corner_type){data_.active_corner_type = corner_type; ui_->checkbox_show_corners->setChecked(data_.show_corners[corner_type]);});
    connect(ui_->selector_comparison_set, qOverload<int>(&QComboBox::activated), [this](int set_idx){data_.comparison_set_idx = set_idx; updateCompCorners();});
    connect(ui_->checkbox_show_corners, &QCheckBox::clicked, [this](bool checked){data_.show_corners[ui_->selector_visu_corners->currentIndex()] = checked; emit updateImg();});
    connect(ui_->checkbox_show_lens_image_bounds, &QCheckBox::clicked, [this](bool checked){data_.show_microlens_image_bounds = checked; emit updateImg();});
    connect(ui_->checkbox_show_MLA_centers, &QCheckBox::clicked, [this](bool checked){data_.show_MLA_centers = checked; emit updateImg();});
    connect(ui_->checkbox_show_comparison_corners, &QCheckBox::clicked, [this](bool checked){data_.show_comparison_corners = checked; emit updateCompCorners();});
    connect(ui_->button_color_bounds, &QPushButton::clicked, this, &Main_Window::updateColors);
    connect(ui_->button_color_corners, &QPushButton::clicked, this, &Main_Window::updateColors);
    connect(ui_->button_color_MLA_centers, &QPushButton::clicked, this, &Main_Window::updateColors);
    connect(ui_->button_color_comparison_corners, &QPushButton::clicked, this, &Main_Window::updateColors);
    connect(ui_->button_compare_cornersets, &QPushButton::clicked, &analyzer_, &Analyzer::compareActiveSets);

    connect(this, &Main_Window::colorSelected, ui_->image_view, &Qt_Tools::Graphics_View::changeElementColor);
    connect(this, &Main_Window::updateCompCorners, ui_->image_view, &Qt_Tools::Graphics_View::updateComparisonCorners);
    connect(this, &Main_Window::calculateCorners, &img_ops_, &Image_Ops::findCornersInSingleImage);
    connect(this, &Main_Window::calculateAllCorners, &img_ops_, &Image_Ops::findCornersInImages);

    //connect main window
    connect(this, &Main_Window::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(this, &Main_Window::updateImg, ui_->image_view, &Qt_Tools::Graphics_View::updateImage);
    connect(this, &Main_Window::loadSets, &data_, &Data::loadSets);
    connect(this, &Main_Window::loadMLA, &data_, &Data::loadMLAConfig);

    //connect data
    connect(&data_, &Data::listsUpdated, this, &Main_Window::updateLists);
    connect(&data_, &Data::cornersUpdated, ui_->image_view, &Qt_Tools::Graphics_View::updateImage);
    connect(&data_, &Data::MLAUpdated, ui_->image_view, &Qt_Tools::Graphics_View::updateMLA);
    connect(&data_, &Data::MLAUpdated, [this](){ui_->button_load_sets->setEnabled(true);});
    connect(&data_, &Data::progress, ui_->progressbar, &QProgressBar::setValue);
    connect(&data_, &Data::log, ui_->logger, &Qt_Tools::Logger::log);

    //connect graphics view
    connect(ui_->image_view, &Qt_Tools::Graphics_View::newMousePosition, this, &Main_Window::displayColorValues);
    connect(ui_->image_view, &Qt_Tools::Graphics_View::deleteClick, &data_, &Data::deleteCorner);

    //connect image ops
    connect(&img_ops_, &Image_Ops::imageCornersCalculated, [this](){ui_->button_calculate_corners->setText("Recalculate All Image Corners"); ui_->checkbox_show_corners->setEnabled(true); ui_->button_save_corners->setEnabled(true); emit updateImg();});
    connect(&img_ops_, &Image_Ops::setsUpdated, this, &Main_Window::updateLists);
    connect(&img_ops_, &Image_Ops::setsUpdated, &data_, &Data::updateXML);
    connect(&img_ops_, &Image_Ops::log, ui_->logger, &Qt_Tools::Logger::log);

    //connect analyzer tab
    connect(ui_->button_run_analysis, &QPushButton::clicked, &analyzer_, &Analyzer::runAnalysis);
    connect(ui_->checkbox_show_boxplot_UV, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_BOXPLOT_UV,checked);});
    connect(ui_->checkbox_show_meanmax_UV, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_MEANMAX_UV,checked);});
    connect(ui_->checkbox_show_detectrate_UV, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_DETECTRATE_UV,checked);});
    connect(ui_->checkbox_show_meanvar_UV, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_MEANVAR_UV,checked);});
    connect(ui_->checkbox_show_boxplot_3D, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_BOXPLOT_3D,checked);});
    connect(ui_->checkbox_show_meanmax_3D, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_MEANMAX_3D,checked);});
    connect(ui_->checkbox_show_detectrate_3D, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_DETECTRATE_3D,checked);});
    connect(ui_->checkbox_show_meanvar_3D, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_MEANVAR_3D,checked);});
    connect(ui_->checkbox_show_method_compare, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_INTER_METHOD,checked);});
    connect(ui_->checkbox_show_mean_compare, &QCheckBox::clicked, [this](const bool checked){ui_->chart->toggleSeries(SERIES_INTER_METHOD_MEAN,checked);});
    connect(ui_->checkbox_analyze_only_common, &QCheckBox::clicked, [this](const bool checked){data_.analyze_only_common = checked;});
    connect(ui_->spinbox_fontsize, qOverload<int>(&QSpinBox::valueChanged), ui_->chart, &Qt_Tools::Chart::setFontSize);
    connect(ui_->spinbox_fontsize, qOverload<int>(&QSpinBox::valueChanged), ui_->legend, &Qt_Tools::Legend::setFontSize);
    connect(ui_->spinbox_linewidth, qOverload<int>(&QSpinBox::valueChanged), ui_->chart, &Qt_Tools::Chart::setPenWidth);
    connect(ui_->button_save_chart, &QPushButton::clicked, this, &Main_Window::saveCurrentChart);

    connect(ui_->chart, &Qt_Tools::Chart::setLegendVisibility, ui_->legend, &Qt_Tools::Legend::setVisibility);
    connect(ui_->chart, &Qt_Tools::Chart::setLegendData, ui_->legend, &Qt_Tools::Legend::addData);
    connect(ui_->chart, &Qt_Tools::Chart::chartDataCreated, this, &Main_Window::setChartVisibility);


    //connect analyzer
    connect(&analyzer_, &Analyzer::analysisCompleted, ui_->chart, &Qt_Tools::Chart::showAnalysis);
    connect(&analyzer_, &Analyzer::log, ui_->logger, &Qt_Tools::Logger::log);

    //connect menu bar
    connect(ui_->action_combine_datasets, &QAction::triggered, [](){Temp::fuseImages();});

    //start threads
    thread_data_->start();
    thread_img_ops_->start();
    thread_analyzer_->start();
}

Main_Window::~Main_Window(){
    thread_data_->exit();
    thread_img_ops_->exit();
    delete ui_;
}

void Main_Window::setThresholds(){
    bool check = true;
    QString lambda_a_text = ui_->line_edit_lambda_a->text();
    float lambda_a = lambda_a_text.toFloat(&check);
    if(check){
        data_.lambda_a = lambda_a;
    }
    else{
        ERROR("Invalid value for lambda_a given. Using "+QString::number(data_.lambda_a));
        ui_->line_edit_lambda_a->setText(QString::number(data_.lambda_a));
    }

    QString lambda_d_text = ui_->line_edit_lambda_d->text();
    float lambda_d = lambda_d_text.toFloat(&check);
    if(check){
        data_.lambda_d = lambda_d;
    }
    else{
        ERROR("Invalid value for lambda_d given. Using "+QString::number(data_.lambda_d));
        ui_->line_edit_lambda_a->setText(QString::number(data_.lambda_d));
    }

    QString init_thresh_text = ui_->line_edit_init_thresh->text();
    float init_thresh = init_thresh_text.toFloat(&check);
    if(check){
        data_.gt_init_threshold = init_thresh;
    }
    else{
        ERROR("Invalid value for initial threshold given. Using "+QString::number(data_.gt_init_threshold));
        ui_->line_edit_init_thresh->setText(QString::number(data_.gt_init_threshold));
    }

    data_.num_threads = ui_->spinbox_threads->value();
}

void Main_Window::loadImageSets(){
    //ask user to select set file
    QString config_file = QFileDialog::getOpenFileName(this,"Select the image set configuration file",data_.last_dir_path,"*.xml");
    if(config_file.size() == 0){
        STATUS("No file selected");
        return;
    }
    else{
        emit loadSets(config_file);
    }
}

void Main_Window::loadMLAConfig(){
    //ask user to select MLA config file
    QString config_file = QFileDialog::getOpenFileName(this,"Select the MLA configuration file",data_.last_dir_path,"*.xml");
    if(config_file.size() == 0){
        STATUS("No file selected");
        return;
    }
    else{
        emit loadMLA(config_file);
    }
}

void Main_Window::updateLists(){
    //delete old lists
    while(ui_->list_sets->count()>0){
        ui_->list_sets->takeItem(0);
    }

    ui_->selector_comparison_set->blockSignals(true);
    ui_->selector_comparison_set->clear();

    bool normalized_available = true;
    bool any_corners_available = false;

    //add list items to set list
    for(const Set& set: data_.sets){
        QListWidgetItem* item = new QListWidgetItem(set.name);
        ui_->list_sets->insertItem(ui_->list_sets->count(),item);
        //check if normalized images are available
        normalized_available = (set.normalized_image_name.size() != 0);
        if(set.uv_image_name.size()!= 0){
            normalized_available &= (set.uv_normalized_image_name.size() != 0);
        }
        if(set.pos_3d_image_name.size()!= 0){
            normalized_available &= (set.pos_3d_normalized_image_name.size() != 0);
        }
        if(set.pos_3d_image_name.size()!= 0){
            normalized_available &= (set.pos_3d_normalized_image_name.size() != 0);
        }
        if(set.pos_plane_far_image_name.size()!= 0){
            normalized_available &= (set.pos_plane_far_normalized_image_name.size() != 0);
        }
        if(set.pos_plane_near_image_name.size()!= 0){
            normalized_available &= (set.pos_plane_near_normalized_image_name.size() != 0);
        }

        //check if corner files are available
        if(set.detected_gt_corners.size() != 0){
            any_corners_available = true;
        }

        ui_->selector_comparison_set->addItem(set.name);
    }

    //enable GUI elements depending on the state of the added items
    ui_->selector_comparison_set->setCurrentIndex(-1);
    ui_->selector_comparison_set->blockSignals(false);
    data_.comparison_set_idx = -1;

    ui_->groupbox_sets->setEnabled(true);
    ui_->groupbox_visualization->setEnabled(true);
    ui_->groupbox_corner_detection->setEnabled(true);

    if(any_corners_available){
        ui_->checkbox_show_corners->setEnabled(true);
        ui_->button_save_corners->setEnabled(true);
    }
    if(!normalized_available){
        ui_->button_generate_normalized_images->setEnabled(true);
    }
    else{
        ui_->button_generate_normalized_images->setEnabled(false);
        if(any_corners_available){
            ui_->button_calculate_corners->setText("Recalculate All Image Corners");
        }
    }
}

void Main_Window::listItemClicked(){

    int image_type_version_code = 3*ui_->selector_image_type->currentIndex()+ui_->selector_image_version->currentIndex();
    data_.image_type_description = data_.image_type_descriptions.at(image_type_version_code);

    if(sender() == ui_->list_sets){
        data_.current_set_index = (ui_->list_sets->currentRow() == -1) ? 0 : ui_->list_sets->currentRow();
    }
    QString file_name = data_.set()->getPath(image_type_version_code);
    if(file_name.size() == 0){
        WARN("The "+data_.image_type_description+" of this set does not exist yet.");
        return;
    }
    file_name = data_.set()->dir_path+file_name;

    //check if file exists
    QFile file(file_name);
    if(!file.exists()){
        ERROR("The "+data_.image_type_description+" file does not exist.");
        return;
    }

    data_.current_image = cv::imread(file_name.toStdString(),cv::IMREAD_UNCHANGED);
    emit updateImg();
}

void Main_Window::updateColors(){
    if(sender() == ui_->button_color_bounds){
        QColor color = QColorDialog::getColor(data_.pen_projected_mla_centers->color(),nullptr,"Select a Color", QColorDialog::ShowAlphaChannel);
        if(color.isValid()){
            data_.pen_projected_mla_centers->setColor(color);
            emit colorSelected(ELEMENT_ML_BOUNDS);
        }
    }
    else if(sender() == ui_->button_color_corners){
        int idx = ui_->selector_visu_corners->currentIndex();
        QColor color = QColorDialog::getColor(data_.pen_corners[idx]->color(),nullptr,"Select a Color", QColorDialog::ShowAlphaChannel);
        if(color.isValid()){
            data_.pen_corners[idx]->setColor(color);
            emit colorSelected(idx);
        }
    }
    else if(sender() == ui_->button_color_MLA_centers){
        QColor color = QColorDialog::getColor(data_.pen_mla_centers->color(),nullptr,"Select a Color", QColorDialog::ShowAlphaChannel);
        if(color.isValid()){
            data_.pen_mla_centers->setColor(color);
            emit colorSelected(ELEMENT_MLA_CENTERS);
        }
    }

    else if(sender() == ui_->button_color_comparison_corners){
        QColor color = QColorDialog::getColor(data_.pen_comparison_corners->color(),nullptr,"Select a Color", QColorDialog::ShowAlphaChannel);
        if(color.isValid()){
            data_.pen_comparison_corners->setColor(color);
            emit colorSelected(ELEMENT_CORNERS_COMPARISON);
        }
    }
}

void Main_Window::displayColorValues(const int& x, const int& y){
    if(0<=x && x<data_.current_image.cols && 0<=y && y<data_.current_image.rows){
        ui_->display_mouse->setText(QString::number(x)+" / "+QString::number(y));

        if(data_.current_image.type() == CV_8UC4){
            cv::Vec4b color = data_.current_image.at<cv::Vec4b>(y,x);
            setDisplays(color[2],color[1],color[0],color[3]);
        }
        else if(data_.current_image.type() == CV_8UC3){
            cv::Vec3b color = data_.current_image.at<cv::Vec3b>(y,x);
            setDisplays(color[2],color[1],color[0],-1);
        }
        if(data_.current_image.type() == CV_16UC4){
            cv::Vec4w color = data_.current_image.at<cv::Vec4w>(y,x);
            setDisplays(color[2],color[1],color[0],color[3]);
        }
        else if(data_.current_image.type() == CV_16UC3){
            cv::Vec3w color = data_.current_image.at<cv::Vec3w>(y,x);
            setDisplays(color[2],color[1],color[0],-1);
        }
        else if(data_.current_image.type() == CV_32FC3){
            cv::Vec3f color = data_.current_image.at<cv::Vec3f>(y,x);
            setDisplays(color[2],color[1],color[0],-1.0);
        }
    }

    if(data_.set() != nullptr){
        int lens_type = (data_.current_image_type == IMAGE_TYPE_UV) ? data_.set()->grid_projected_lens_centers_uv->getLensType(cv::Point2f(x,y)) : data_.set()->grid_projected_lens_centers->getLensType(cv::Point2f(x,y));
        ui_->display_lens_type->setText(QString::number(lens_type));
        if(lens_type == 0){
            ui_->display_lens_f->setText(QString::number(data_.set()->mla->lens_1_f));
        }
        else if(lens_type == 1){
            ui_->display_lens_f->setText(QString::number(data_.set()->mla->lens_2_f));
        }
        else if(lens_type == 2){
            ui_->display_lens_f->setText(QString::number(data_.set()->mla->lens_3_f));
        }
    }
}

void Main_Window::setDisplays(const float& r, const float& g, const float& b, const float& a){
    ui_->display_R->setText(QString::number(r));
    ui_->display_G->setText(QString::number(g));
    ui_->display_B->setText(QString::number(b));
    if(a==-1.0f){
        ui_->display_A->setText("-");
    }
    else{
        ui_->display_A->setText(QString::number(a));
    }
}

void Main_Window::setDisplays(const int& r, const int& g, const int& b, const int& a){
    ui_->display_R->setText(QString::number(r));
    ui_->display_G->setText(QString::number(g));
    ui_->display_B->setText(QString::number(b));
    if(a==-1){
        ui_->display_A->setText("-");
    }
    else{
        ui_->display_A->setText(QString::number(a));
    }
}

void Main_Window::saveCurrentChart(){
    //ask user for file name
    QString path = QFileDialog::getSaveFileName(nullptr,"Choose a file name to save the chart",data_.last_dir_path);
    if(path.size() == 0){
        return;
    }
    //add file type
    QString test = path.mid(path.size()-3);
    if(path.mid(path.size()-3).compare("png") != 0){
        path+=".png";
    }
    //get image and save in the specified location
    QPixmap image = ui_->frame_chart->grab();
    image.save(path, "PNG");
}

void Main_Window::setChartVisibility(){
    ui_->checkbox_show_boxplot_UV->clicked(ui_->checkbox_show_boxplot_UV->checkState());
    ui_->checkbox_show_meanmax_UV->clicked(ui_->checkbox_show_meanmax_UV->checkState());
    ui_->checkbox_show_detectrate_UV->clicked(ui_->checkbox_show_detectrate_UV->checkState());
    ui_->checkbox_show_meanvar_UV->clicked(ui_->checkbox_show_meanvar_UV->checkState());
    ui_->checkbox_show_boxplot_3D->clicked(ui_->checkbox_show_boxplot_3D->checkState());
    ui_->checkbox_show_meanmax_3D->clicked(ui_->checkbox_show_meanmax_3D->checkState());
    ui_->checkbox_show_detectrate_3D->clicked(ui_->checkbox_show_detectrate_3D->checkState());
    ui_->checkbox_show_meanvar_3D->clicked(ui_->checkbox_show_meanvar_3D->checkState());
    ui_->checkbox_show_method_compare->clicked(ui_->checkbox_show_method_compare->checkState());
    ui_->checkbox_show_mean_compare->clicked(ui_->checkbox_show_mean_compare->checkState());
}
