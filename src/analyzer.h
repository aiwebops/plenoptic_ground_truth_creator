#pragma once

#include "data.h"
#include "logger.h"

class Analyzer : public QObject{

    Q_OBJECT

public:
    Analyzer(Data* data);

public slots:
    void compareActiveSets();
    void runAnalysis();

private:
    Data* data_;

    ///
    /// \brief compareSets
    /// \param set_1
    /// \param set_2
    /// \param result
    /// \param reference
    /// \param method
    /// \return
    ///
    bool compareSets(const Set& set_1, const Set& set_2, Comparison_Data* result, const int& method, const points2D& reference = points2D());

    ///
    /// \brief compareMethods
    /// \param set
    /// \param mean_var
    /// \return
    ///
    bool compareMethods(const Set& set, pairf* mean_var);

    ///
    /// \brief analyzseNames
    /// \param analysis
    /// \return
    ///
    bool analyzeNames(Analysis* analysis);

signals:
    void log(const QString& msg, const int& type);
    void analysisCompleted(const Analysis& analysis);
};
