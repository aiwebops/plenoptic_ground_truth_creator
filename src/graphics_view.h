#pragma once

#include "data.h"

#include <opencv2/opencv.hpp>

#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QScrollBar>
#include <QTimer>

namespace Qt_Tools{

class Graphics_View : public QGraphicsView
{
    Q_OBJECT

public:
    ///
    /// \brief Graphics_View constructor
    ///
    Graphics_View(QWidget* parent = nullptr);

    void setData(Data* data){
        data_ = data;
    }

public slots:
    ///
    /// \brief setCvImage sets the image to display
    /// \param image    Image to display
    ///
    void setCvImage(const cv::Mat& image);

    ///
    /// \brief setImage sets the image to display
    /// \param image    Image to display
    ///
    void setImage(const QPixmap& image);

    ///
    /// \brief updateImage
    ///
    void updateImage();

    ///
    /// \brief updateComparisonCorners
    ///
    void updateComparisonCorners();

    ///
    /// \brief updateMLA
    ///
    void updateMLA();

    ///
    /// \brief changeElementColor
    /// \param element_code
    ///
    void changeElementColor(const int element_code);

private:
    Data* data_;
    QGraphicsScene* scene_;
    QGraphicsPixmapItem* item_;
    std::vector<QGraphicsItemGroup*> corners_;
    QGraphicsItemGroup* corners_comparison_;
    QGraphicsItemGroup* projected_mla_centers_;
    QGraphicsItemGroup* mla_centers_;

    QPen active_circle_pen_;
    QGraphicsEllipseItem* active_circle_ = nullptr;

    Hex_Grid* current_grid = nullptr;
    Hex_Grid* current_grid_projected = nullptr;

    //save last mouse position and scroll bar positions
    QPoint last_cursor_position_;
    int last_h_bar_pos_;
    int last_v_bar_pos_;
    //current image zoom level
    float zoom_ = 1.0;
    //saves if the left mouse button is currently pressed
    bool mouse_pressed_ = false;
    //Timer used in order to reduce the mouse move event calculation rate
    QTimer timer_;

    ///
    /// \brief mousePressEvent handles mouse clicks for starting a move in a zoomed in image
    /// \param event    Mouse event
    ///
    void mousePressEvent(QMouseEvent* event);

    ///
    /// \brief mouseReleaseEvent handles mouse releases for ending a move in a zoomed in image
    /// \param event    Mouse event
    ///
    void mouseReleaseEvent(QMouseEvent* event);

    ///
    /// \brief mouseMoveEvent handles mouse move events for moving in a zoomed in image
    /// \param event    Mouse event
    ///
    void mouseMoveEvent(QMouseEvent* event);

    ///
    /// \brief wheelEvent handles scrolling events for zooming into or out of an image
    /// \param event    Wheel event
    ///
    void wheelEvent(QWheelEvent* event);

signals:
    void imageSet();
    void newMousePosition(const int& x, const int&y);
    void deleteClick(const int& x, const int& y);
};
}


