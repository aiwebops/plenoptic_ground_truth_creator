#include "temp.h"

#include <QFileDialog>

using namespace cv;
using namespace std;

void Temp::fuseImages(){
    //ask user for dirs
    QString start_dir = "/home/tmichels/Desktop/plenoptic_checkerboard_data";
    vector<QString> dir_list;
    QString new_dir_path = QFileDialog::getExistingDirectory(nullptr, "Select the set directories", start_dir);
    while(new_dir_path.size() > 0){
        dir_list.push_back(new_dir_path);
        new_dir_path = QFileDialog::getExistingDirectory(nullptr, "Select the set directories", start_dir);
    }

    QString target_dir = QFileDialog::getExistingDirectory(nullptr,"Select a target directory",start_dir);

    //scan for images
    int num_images = 0;
    vector<vector<QString>> image_lists;
    for(QString& dir_path : dir_list){
        image_lists.push_back(vector<QString>());
        dir_path+="/data/";
        QDir dir(dir_path);
        QStringList entries = dir.entryList({"*.exr"});
        for(const QString& entry : entries){
            image_lists.back().push_back(entry);
        }
        num_images += image_lists.back().size();
    }

    //scan for images with identical names and fuse those
    while(num_images > 0){

        //get first image name
        QString current_image_name;
        for(size_t i = 0; i < dir_list.size(); i++){
            vector<QString>& list = image_lists[i];
            if(list.size() > 0){
                current_image_name = list.at(0);
                break;
            }
        }

        //search in all lists and save the paths
        vector<QString> image_paths;
        for(size_t i = 0; i < dir_list.size(); i++){
            const QString& dir_path = dir_list[i];
            vector<QString>& list = image_lists[i];
            for(size_t j = 0; j < list.size(); j++){
                const QString& comp_image_name = list[j];
                if(comp_image_name.compare(current_image_name) == 0){
                    image_paths.push_back(dir_path+current_image_name);
                    //delete image from list
                    list.erase(list.begin()+j);
                    j--;
                    num_images--;
                }
            }
        }

        //fuse the found image
        Mat result(0,0,CV_8U);
        for(const QString& image_path : image_paths){
            Mat current_image = imread(image_path.toStdString(),IMREAD_UNCHANGED);
            if(result.rows == 0){
                //the first image is just copied
                result = current_image.clone();
            }
            else{
                result += current_image;
            }
        }

        //write the image to the target dir
        string target_image_path = target_dir.toStdString()+"/data/"+current_image_name.toStdString();
        imwrite(target_image_path,result);
    }
}

