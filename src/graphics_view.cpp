#include "graphics_view.h"

using namespace std;
using namespace cv;

Qt_Tools::Graphics_View::Graphics_View(QWidget *parent){
    this->setParent(parent);
    viewport()->setMouseTracking(true); //activate mouse tracking
    timer_.setInterval(25); //40 hz update rate for mouse movements
    timer_.setSingleShot(true);

    item_ = new QGraphicsPixmapItem();
    scene_ = new QGraphicsScene();
    corners_.push_back(new QGraphicsItemGroup());
    corners_.push_back(new QGraphicsItemGroup());
    corners_comparison_ = new QGraphicsItemGroup();
    mla_centers_ = new QGraphicsItemGroup();
    projected_mla_centers_ = new QGraphicsItemGroup();

    scene_->addItem(item_);
    scene_->addItem(corners_[0]);
    scene_->addItem(corners_[1]);
    scene_->addItem(corners_comparison_);
    scene_->addItem(mla_centers_);
    scene_->addItem(projected_mla_centers_);
    setScene(scene_);
}

void Qt_Tools::Graphics_View::setCvImage(const cv::Mat& image){
    cv::Mat image_copy;
    QImage::Format format;
    if(image.type() == CV_8UC1){
        format = QImage::Format_Grayscale8;
    }
    else if(image.type() == CV_8UC3){
        format = QImage::Format_RGB888;
    }
    else if(image.type() == CV_8UC4){
        format = QImage::Format_RGBA8888;
    }
    else if(image.type() == CV_32F){//usually a depth image
        format = QImage::Format_RGB888;
        Mat colored_image;

        image.convertTo(colored_image,CV_8UC1, 255.0/6000.0, 0);
        applyColorMap(colored_image,colored_image,COLORMAP_JET);
        QImage qImage(colored_image.data, colored_image.cols, colored_image.rows, colored_image.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else if(image.type() == CV_16UC3){
        image.convertTo(image_copy,CV_8UC3, 1.0/256.0);
        format = QImage::Format_RGB888;
        QImage qImage(image_copy.data, image_copy.cols, image_copy.rows, image_copy.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else if(image.type() == CV_16UC4){
        image.convertTo(image_copy,CV_8UC4, 1.0/256.0);
        format = QImage::Format_RGBA8888;
        QImage qImage(image_copy.data, image_copy.cols, image_copy.rows, image_copy.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else if(image.type() == CV_32FC3){
        image.convertTo(image_copy,CV_8UC3, 1500.0);
        format = QImage::Format_RGB888;
        QImage qImage(image_copy.data, image_copy.cols, image_copy.rows, image_copy.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else{
        return;
    }

    QImage qImage(image.data, image.cols, image.rows, image.step, format);
    setImage(QPixmap::fromImage(qImage));

    emit imageSet();
}

void Qt_Tools::Graphics_View::setImage(const QPixmap& image){
    item_->setPixmap(image);
}

void Qt_Tools::Graphics_View::updateImage(){
    //set current image
    if(data_->current_set_index != -1 && data_->current_image_type != -1){
        setCvImage(data_->current_image);
    }
    else{
        return;
    }
    //remove old markers
    scene_->removeItem(corners_[0]);
    scene_->removeItem(corners_[1]);
    corners_[0] = new QGraphicsItemGroup();
    corners_[1] = new QGraphicsItemGroup();

    for(size_t i = 0; i < data_->show_corners.size(); i++){
        if(data_->show_corners[i] && data_->set()->detected_gt_corners[i].size() != 0){
            const vector<vector<Point2f>>& corner_points = data_->set()->detected_gt_corners[i];
            if(data_->current_image_type == IMAGE_TYPE_UV){
                float scaling = data_->set()->scale;
                float offset = (scaling-1.0)/2.0;
                for(const vector<Point2f>& single_corner_occurrences : corner_points){
                    for(const Point2f& corner : single_corner_occurrences){
                        //scale corners since these are saved in unscaled checkerboard coordinates
                        QGraphicsEllipseItem* point = new QGraphicsEllipseItem((corner.x*scaling+offset)-1.0,(corner.y*scaling+offset)-1.0,3.0,3.0);
                        point->setPen(*data_->pen_corners[i]);
                        corners_[i]->addToGroup(point);
                    }
                }
            }
            else{
                for(const vector<Point2f>& single_corner_occurrences : corner_points){
                    for(const Point2f& corner : single_corner_occurrences){
                        QGraphicsEllipseItem* point = new QGraphicsEllipseItem(corner.x-1.0,corner.y-1.0,3.0,3.0);
                        point->setPen(*data_->pen_corners[i]);
                        corners_[i]->addToGroup(point);
                    }
                }
            }
        }
        //add markers to scene
        scene_->addItem(corners_[i]);
    }
    updateMLA();
    updateComparisonCorners();
}

void Qt_Tools::Graphics_View::updateComparisonCorners(){
    //check if image is shown
    if(data_->current_set_index == -1 || data_->current_image_type == -1){
        return;
    }

    //remove old markers
    scene_->removeItem(corners_comparison_);
    corners_comparison_ = new QGraphicsItemGroup();

    if(data_->show_comparison_corners && data_->comparisonSet() != nullptr){
        const vector<vector<Point2f>>& corner_points = data_->comparisonSet()->detected_gt_corners[0];
        if(data_->current_image_type == IMAGE_TYPE_UV){
            float scaling = data_->comparisonSet()->scale;
            float offset = (scaling-1.0)/2.0;
            for(const vector<Point2f>& single_corner_occurrences : corner_points){
                for(const Point2f& corner : single_corner_occurrences){
                    //scale corners since these are saved in unscaled checkerboard coordinates
                    QGraphicsEllipseItem* point = new QGraphicsEllipseItem((corner.x*scaling+offset)-1.0,(corner.y*scaling+offset)-1.0,3.0,3.0);
                    point->setPen(*data_->pen_comparison_corners);
                    corners_comparison_->addToGroup(point);
                }
            }
        }
        else{
            for(const vector<Point2f>& single_corner_occurrences : corner_points){
                for(const Point2f& corner : single_corner_occurrences){
                    QGraphicsEllipseItem* point = new QGraphicsEllipseItem(corner.x-1.0,corner.y-1.0,3.0,3.0);
                    point->setPen(*data_->pen_comparison_corners);
                    corners_comparison_->addToGroup(point);
                }
            }
        }
    }
    //add markers to scene
    scene_->addItem(corners_comparison_);
}


void Qt_Tools::Graphics_View::updateMLA(){
    //check if image is set
    if(data_->current_set_index == -1 || data_->current_image_type == -1 || data_->set()->mla == nullptr){
        return;
    }

    bool only_change_state = false;

    if(data_->current_image_type == IMAGE_TYPE_RENDER || data_->current_image_type == IMAGE_TYPE_3D){
        //check if grids exist..
        if(data_->set()->grid_lens_centers == nullptr || data_->set()->grid_lens_centers == nullptr){
            return;
        }
        //or if identical to already shown grids
        else if(data_->set()->grid_lens_centers == current_grid && data_->set()->grid_projected_lens_centers == current_grid_projected){
            only_change_state = true;
        }
        else{
            current_grid = data_->set()->grid_lens_centers;
            current_grid_projected = data_->set()->grid_projected_lens_centers;
        }
    }
    else if(data_->current_image_type == IMAGE_TYPE_UV){
        //check if grids exist..
        if(data_->set()->grid_lens_centers_uv == nullptr || data_->set()->grid_projected_lens_centers_uv == nullptr){
            return;
        }
        //or if identical to already shown grids
        else if(data_->set()->grid_lens_centers_uv == current_grid && data_->set()->grid_projected_lens_centers_uv == current_grid_projected){
            only_change_state = true;
        }
        else{
            current_grid = data_->set()->grid_lens_centers_uv;
            current_grid_projected = data_->set()->grid_projected_lens_centers_uv;
        }
    }

    if(!only_change_state){
        //if grids changed, delete the old ones
        scene_->removeItem(mla_centers_);
        mla_centers_ = new QGraphicsItemGroup();
        scene_->removeItem(projected_mla_centers_);
        projected_mla_centers_ = new QGraphicsItemGroup();

        std::vector<cv::Point2f>* grid_points = current_grid->points();
        std::vector<cv::Point2f>* grid_points_projected = current_grid_projected->points();

        const float diam = 3.0;

        //create mla center circles
        for(const Point2f& p : *grid_points){
            QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(p.x-(diam-1.0)/2.0,p.y-(diam-1.0)/2.0,diam,diam);
            circle->setPen(*data_->pen_mla_centers);
            mla_centers_->addToGroup(circle);
        }

        //calculate diameter of microlens images based on the distances in the hex grid
        float scaled_diam;
        if(data_->current_image_type == IMAGE_TYPE_RENDER){
            scaled_diam = data_->set()->mla->mli_center_distance * float(data_->set()->resolution_x)/data_->set()->mla->sensor_width;
        }
        else{
            scaled_diam = data_->set()->mla->mli_center_distance * float(data_->set()->resolution_uv_x)/data_->set()->mla->sensor_width;
        }

        //create microlens image bounds center circles
        for(const Point2f& p : *grid_points_projected){
            QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(p.x-(scaled_diam-1.0)/2.0,p.y-(scaled_diam-1.0)/2.0,scaled_diam,scaled_diam);
            circle->setPen(*data_->pen_projected_mla_centers);
            projected_mla_centers_->addToGroup(circle);
        }

        scene_->addItem(mla_centers_);
        scene_->addItem(projected_mla_centers_);
    }

    //show or hide the new grids depending on the saved checkbox values
    data_->show_MLA_centers ? mla_centers_->show() : mla_centers_->hide();
    projected_mla_centers_->setZValue(100.0);
    data_->show_microlens_image_bounds ? projected_mla_centers_->show() : projected_mla_centers_->hide();
}

void Qt_Tools::Graphics_View::changeElementColor(const int element_code){
    if(element_code == ELEMENT_CORNERS_M1){
        QList<QGraphicsItem*> list = corners_[0]->childItems();
        for(int i = 0; i < list.size(); i++){
            QGraphicsEllipseItem* item = static_cast<QGraphicsEllipseItem*>(list.at(i));
            item->setPen(*data_->pen_corners[0]);
        }
    }
    else if(element_code == ELEMENT_CORNERS_M2){
        QList<QGraphicsItem*> list = corners_[1]->childItems();
        for(int i = 0; i < list.size(); i++){
            QGraphicsEllipseItem* item = static_cast<QGraphicsEllipseItem*>(list.at(i));
            item->setPen(*data_->pen_corners[1]);
        }
    }
    else if(element_code == ELEMENT_CORNERS_COMPARISON){
        QList<QGraphicsItem*> list = corners_comparison_->childItems();
        for(int i = 0; i < list.size(); i++){
            QGraphicsEllipseItem* item = static_cast<QGraphicsEllipseItem*>(list.at(i));
            item->setPen(*data_->pen_comparison_corners);
        }
    }
    else if(element_code == ELEMENT_MLA_CENTERS){
        QList<QGraphicsItem*> list = mla_centers_->childItems();
        for(int i = 0; i < list.size(); i++){
            QGraphicsEllipseItem* item = static_cast<QGraphicsEllipseItem*>(list.at(i));
            item->setPen(*data_->pen_mla_centers);
        }
    }
    else if(element_code == ELEMENT_ML_BOUNDS){
        QList<QGraphicsItem*> list = projected_mla_centers_->childItems();
        for(int i = 0; i < list.size(); i++){
            QGraphicsEllipseItem* item = static_cast<QGraphicsEllipseItem*>(list.at(i));
            item->setPen(*data_->pen_projected_mla_centers);
        }
    }
}

void Qt_Tools::Graphics_View::mousePressEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        //set initial positions of cursor and scrollbars
        last_cursor_position_ = event->globalPos();
        last_h_bar_pos_ = this->horizontalScrollBar()->value();
        last_v_bar_pos_ = this->verticalScrollBar()->value();
        mouse_pressed_ = true;
    }
    else if(event->button() == Qt::MiddleButton){
        //this means the user wants to delete a corner
        QPointF clicked_point_f = this->mapToScene(event->pos());
        emit deleteClick(int(clicked_point_f.x()),int(clicked_point_f.y()));
    }
}

void Qt_Tools::Graphics_View::mouseReleaseEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        mouse_pressed_ = false;
    }
}

void Qt_Tools::Graphics_View::mouseMoveEvent(QMouseEvent* event){
    if(timer_.isActive()){//return in order to prevent signal flooding
        return;
    }

    if(mouse_pressed_){
        //translate scene by modifying scrollbars
        this->horizontalScrollBar()->setValue(last_h_bar_pos_ - (event->globalX()-last_cursor_position_.x()));
        this->verticalScrollBar()->setValue(last_v_bar_pos_ - (event->globalY()-last_cursor_position_.y()));

        //update current positions - needed in order to prevent the mouse from entering a dead (no translation) area
        last_cursor_position_ = event->globalPos();
        last_h_bar_pos_ = this->horizontalScrollBar()->value();
        last_v_bar_pos_ = this->verticalScrollBar()->value();
    }
    else{
        QPointF clicked_point_f = this->mapToScene(event->pos());
        QGraphicsItem* item = (QGraphicsItem*)itemAt(event->pos());
        if(item != nullptr && item != active_circle_ && item->type() == 4){
            if(active_circle_ != nullptr){
                active_circle_->setPen(active_circle_pen_);
            }
            active_circle_ = static_cast<QGraphicsEllipseItem*>(item);
            active_circle_pen_ = active_circle_->pen();
            QPen* highlighted = new QPen(QColor(255,255,255,255));
            active_circle_->setPen(*highlighted);
        }
        emit newMousePosition(int(clicked_point_f.x()),int(clicked_point_f.y()));
    }
    timer_.start();
}

void Qt_Tools::Graphics_View::wheelEvent(QWheelEvent* event){
    //rescale the scene
    float old_zoom = zoom_;
    zoom_ += 0.001f*(float)event->delta();
    this->scale(zoom_/old_zoom,zoom_/old_zoom);
    zoom_ = old_zoom;
}
