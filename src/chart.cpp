#include "chart.h"

Qt_Tools::Chart::Chart(QWidget *parent){

    this->setParent(parent);

    //create axes
    QPen axisPen(QColor(0,0,0));
    axisPen.setWidth(2);

    axis_x_ = new QValueAxis();
    axis_x_->setGridLineVisible(false);

    axis_x_boxes_ = new QValueAxis();
    axis_x_boxes_->setGridLineVisible(false);

    axis_x_cat_ = new QCategoryAxis();
    axis_x_cat_->setLinePen(axisPen);
    axis_x_cat_->setGridLineVisible(true);

    axis_y_ = new QValueAxis();
    axis_y_->setTitleText("Error in px");
    axis_y_->setLinePen(axisPen);
    axis_y_->setGridLineVisible(true);
    axis_y_->setLabelFormat("%.3f  ");

    axis_y_2_ = new QValueAxis();
    axis_y_2_->setTitleText("Detected Corners in %");
    axis_y_2_->setLinePen(axisPen);
    axis_y_2_->setGridLineVisible(true);
    axis_y_2_->setLabelFormat(QString(" . ")+"%3i");

    //set axes to chart
    chart_ = new QtCharts::QChart();
    chart_->addAxis(axis_x_cat_, Qt::AlignBottom);
    chart_->addAxis(axis_x_, Qt::AlignBottom);
    chart_->addAxis(axis_x_boxes_, Qt::AlignBottom);
    chart_->addAxis(axis_y_, Qt::AlignLeft);
    chart_->addAxis(axis_y_2_, Qt::AlignRight);

    axis_x_->hide();
    axis_x_boxes_->hide();

    chart_->legend()->hide();
    this->setChart(chart_);

    //create overlay label
    info_overlay_ = new QLabel(this);

    //create a dummy series in order for the mapping functions to work correctly even before real series are added
    QScatterSeries* series = new QScatterSeries();
    addSeries(series, METHOD_UV_RENDER);

    setChartAxes();
    setFontSize(10);

    //disable rounded chart edges
    chart_->layout()->setContentsMargins(0, 0, 0, 0);
    chart_->setBackgroundRoundness(0);
}

void Qt_Tools::Chart::addSeries(QScatterSeries* series, const int method, const bool& axis_y_left, const QColor& draw_color, const bool save){
    series->setPointsVisible(true);
    series->setColor(draw_color);
    series->setMarkerSize(10.0+line_width_);
    chart_->addSeries(series);
    series->attachAxis(axis_x_);
    if(axis_y_left){
        series->attachAxis(axis_y_);
    }
    else{
        series->attachAxis(axis_y_2_);
    }
    if(save){
        if(method == METHOD_UV_RENDER){
            series_UV_.push_back(series);
        }
        else if(method == METHOD_LINE_INTER){
            series_3D_.push_back(series);
        }
    }
}

void Qt_Tools::Chart::addSeries(QBoxPlotSeries* series, const bool& axis_y_left){
    series->setVisible(true);
    series->setBoxWidth(1.0);
    chart_->addSeries(series);
    series->attachAxis(axis_x_boxes_);
    if(axis_y_left){
        series->attachAxis(axis_y_);
    }
    else{
        series->attachAxis(axis_y_2_);
    }
}

void Qt_Tools::Chart::setChartAxes(const float& x_min, const float& x_max, const int& x_tick, const float& y_min, const float& y_max, const int& y_tick, const float& y_2_min, const float& y_2_max, const int& y_2_tick){
    axis_x_->setMin(x_min);
    axis_x_->setMax(x_max);
    axis_x_->setTickCount(x_tick);

    axis_x_boxes_->setMin(x_min+0.25);
    axis_x_boxes_->setMax(x_max+0.25);
    axis_x_boxes_->setTickCount(x_tick);

    axis_y_->setMin(y_min);
    axis_y_->setMax(y_max);
    axis_y_->setTickCount(y_tick);

    axis_y_2_->setMin(y_2_min);
    axis_y_2_->setMax(y_2_max);
    axis_y_2_->setTickType(QValueAxis::TickType::TicksFixed);
    axis_y_2_->setTickCount(y_2_tick);
}

template<typename T>
void Qt_Tools::Chart::setLabelAxis(const std::vector<T>& labels, const float& num_entries){
    float counter = 1.0;
    for(const T& label : labels){
        axis_x_cat_->append(QString::number(label),counter*num_entries/float(labels.size()));
        counter++;
    }
    axis_x_cat_->setRange(0, num_entries);
}


void Qt_Tools::Chart::showAnalysis(const Analysis& data){

    if(!data.isValid()){
        return;
    }

    /////////////////////////////////////////////////   Chart Setup   /////////////////////////////////////////////////

    //delete old data
    chart_->removeAllSeries();
    series_UV_.clear();
    series_3D_.clear();
    chart_->removeAxis(axis_x_cat_);
    chart_->removeAxis(axis_x_);
    chart_->removeAxis(axis_x_boxes_);

    //add new axis
    QPen axis_pen = axis_x_cat_->linePen();
    QFont axis_font = axis_x_cat_->labelsFont();
    axis_x_cat_ = new QCategoryAxis();
    axis_x_cat_->setLinePen(axis_pen);
    axis_x_cat_->setLabelsFont(axis_font);
    axis_x_cat_->setTitleFont(axis_font);
    axis_x_cat_->setGridLineVisible(true);
    chart_->addAxis(axis_x_cat_, Qt::AlignBottom);
    chart_->addAxis(axis_x_, Qt::AlignBottom);
    chart_->addAxis(axis_x_boxes_, Qt::AlignBottom);

    //adjust chart axes to show data
    if(data.type == ANALYSIS_TYPE_SCALING){
        axis_x_cat_->setTitleText("Scaling Factor K");
        setLabelAxis(data.scalings, float(data.num_sets)+1);
        setChartAxes(-0.5, float(data.num_sets)-1.5, float(data.num_sets), 0.0, 1.1*data.max_val, 11, 0.0, 100.0, 11);
    }
    else if(data.type == ANALYSIS_TYPE_SAMPLES){
        axis_x_cat_->setTitleText("Number of Samples");
        setLabelAxis(data.sample_numbers, float(data.num_sets)+1);
        setChartAxes(-0.5, float(data.num_sets)-1.5, float(data.num_sets), 0.0, 1.1*data.max_val, 11, 0.0, 100.0, 11);
    }
    else if(data.type == ANALYSIS_TYPE_BOTH){
        axis_x_cat_->setTitleText("Scaling Factor K");
        setLabelAxis(data.scalings, float(data.num_sets)*1.2);
        setChartAxes(0.0, float(data.num_sets)*1.2, float(data.num_sets)/5.0+1.0, 0.0, 1.1*data.max_val, 11, 0.0, 100.0, 11);
    }


    /////////////////////////////////////////////////   Series Setup   /////////////////////////////////////////////////

    //create new series
    QScatterSeries* series_max_error_to_best_UV = new QScatterSeries();
    QScatterSeries* series_avg_error_to_best_UV = new QScatterSeries();
    QScatterSeries* series_max_error_to_best_3D = new QScatterSeries();
    QScatterSeries* series_avg_error_to_best_3D = new QScatterSeries();

    series_detect_rate_UV_ = new QScatterSeries();
    series_boxplot_UV_ = new QBoxPlotSeries();
    series_mean_var_UV_ = new QBoxPlotSeries();

    series_detect_rate_3D_ = new QScatterSeries();
    series_boxplot_3D_ = new QBoxPlotSeries();
    series_mean_var_3D_ = new QBoxPlotSeries();

    series_method_compare_ = new QBoxPlotSeries();
    series_method_compare_mean_ = new QScatterSeries();

    int counter = 0;
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(QColor(0,0,0));

    std::vector<QColor> colors = {QColor(0,0,0),QColor(204,0,0),QColor(255,204,0),QColor(0,204,0),QColor(0,180,220),QColor(102,0,204)};

    for(size_t i = 0; i < data.num_sets-1; i++){
        //add empty values between groups
        if(data.type == ANALYSIS_TYPE_BOTH && counter%6 == 0){

            series_max_error_to_best_UV->append(counter,-1.0);
            series_avg_error_to_best_UV->append(counter,-1.0);

            series_max_error_to_best_3D->append(counter,-1.0);
            series_avg_error_to_best_3D->append(counter,-1.0);

            series_boxplot_UV_->append(new QBoxSet(0.0,0.0,0.0,0.0,0.0));
            series_mean_var_UV_->append(new QBoxSet(0.0,0.0,0.0,0.0,0.0));
            series_detect_rate_UV_->append(counter,-1);

            series_boxplot_3D_->append(new QBoxSet(0.0,0.0,0.0,0.0,0.0));
            series_mean_var_3D_->append(new QBoxSet(0.0,0.0,0.0,0.0,0.0));
            series_detect_rate_3D_->append(counter,-1);

            series_method_compare_->append(new QBoxSet(0.0,0.0,0.0,0.0,0.0));
            series_method_compare_mean_->append(counter,-1.0);

            counter++;
        }
        if(data.type == ANALYSIS_TYPE_BOTH){
            brush.setColor(colors[counter%6]);
        }

        const Comparison_Data& comp_best_UV = data.compared_to_best_UV[i];
        const Comparison_Data& comp_best_3D = data.compared_to_best_3D[i];

        series_max_error_to_best_UV->append(counter,comp_best_UV.dist_100);
        series_avg_error_to_best_UV->append(counter,comp_best_UV.avg_dist);
        series_detect_rate_UV_->append(counter,100.0*float(data.num_detected_points[i])/float(data.max_num_detected_points));

        series_max_error_to_best_3D->append(counter,comp_best_3D.dist_100);
        series_avg_error_to_best_3D->append(counter,comp_best_3D.avg_dist);
        series_detect_rate_3D_->append(counter,100.0*float(data.num_detected_points[i])/float(data.max_num_detected_points));

        QBoxSet* box_full_UV;
        QBoxSet* box_mean_var_UV;

        QBoxSet* box_full_3D;
        QBoxSet* box_mean_var_3D;

        QBoxSet* box_compare;


        box_full_UV = new QBoxSet(comp_best_UV.dist_0,comp_best_UV.dist_25,comp_best_UV.median_dist,comp_best_UV.dist_75,comp_best_UV.dist_100);
        box_mean_var_UV = new QBoxSet(comp_best_UV.avg_dist-comp_best_UV.std_dev,comp_best_UV.avg_dist-0.0001,comp_best_UV.avg_dist-0.0001,comp_best_UV.avg_dist+0.0001,comp_best_UV.avg_dist+comp_best_UV.std_dev);
        box_full_3D = new QBoxSet(comp_best_3D.dist_0,comp_best_3D.dist_25,comp_best_3D.median_dist,comp_best_3D.dist_75,comp_best_3D.dist_100);
        box_mean_var_3D = new QBoxSet(comp_best_3D.avg_dist-comp_best_3D.std_dev,comp_best_3D.avg_dist-0.0001,comp_best_3D.avg_dist-0.0001,comp_best_3D.avg_dist+0.0001,comp_best_3D.avg_dist+comp_best_3D.std_dev);

        if(data.compared_methods.size()>0){
            const pairf& comp = data.compared_methods[i];
            box_compare = new QBoxSet(comp.first-comp.second,comp.first-0.000001,comp.first-0.000001,comp.first+0.000001,comp.first+comp.second);
            box_compare->setBrush(brush);
            box_compare->setPen(QPen(brush,line_width_));
            series_method_compare_->append(box_compare);
            series_method_compare_mean_->append(counter, data.compared_means[i]);
        }


        box_full_UV->setBrush(brush);
        box_full_UV->setPen(QPen(brush,line_width_));
        series_boxplot_UV_->append(box_full_UV);
        box_mean_var_UV->setBrush(brush);
        box_mean_var_UV->setPen(QPen(brush,line_width_));
        series_mean_var_UV_->append(box_mean_var_UV);

        box_full_3D->setBrush(brush);
        box_full_3D->setPen(QPen(brush,line_width_));
        series_boxplot_3D_->append(box_full_3D);
        box_mean_var_3D->setBrush(brush);
        box_mean_var_3D->setPen(QPen(brush,line_width_));
        series_mean_var_3D_->append(box_mean_var_3D);

        counter++;
    }
    series_detect_rate_UV_->append(counter,100.0*float(data.num_detected_points[data.num_sets-1])/float(data.max_num_detected_points));
    series_detect_rate_3D_->append(counter,100.0*float(data.num_detected_points[data.num_sets-1])/float(data.max_num_detected_points));

    //show new series
    addSeries(series_max_error_to_best_UV, METHOD_UV_RENDER, true,QColor(255,0,0));
    addSeries(series_avg_error_to_best_UV, METHOD_UV_RENDER, true,QColor(0,160,0));
    //addSeries(series_boxplot_UV_, true);
    //addSeries(series_mean_var_UV_, true);

    addSeries(series_max_error_to_best_3D, METHOD_LINE_INTER, true,QColor(255,0,0));
    addSeries(series_avg_error_to_best_3D, METHOD_LINE_INTER, true,QColor(0,160,0));
    //addSeries(series_boxplot_3D_, true);
    //addSeries(series_mean_var_3D_, true);

    addSeries(series_method_compare_, true);
    addSeries(series_method_compare_mean_, -1, true);
    series_method_compare_mean_->setMarkerShape(QScatterSeries::MarkerShapeRectangle);

    if(data.type == ANALYSIS_TYPE_BOTH){
        addSeries(series_detect_rate_UV_, METHOD_UV_RENDER,false,QColor(0,0,0),false);
        addSeries(series_detect_rate_3D_, METHOD_LINE_INTER,false,QColor(0,0,0),false);

        emit setLegendVisibility(true);
        std::vector<QString> legend_labels = {"Detected Corners"};
        for(const int& sample_num : data.sample_numbers){
            legend_labels.push_back(QString::number(sample_num)+"² Samples");
        }
        emit setLegendData(colors,legend_labels);

    }
    else{
        addSeries(series_detect_rate_UV_, METHOD_UV_RENDER,false,QColor(204,51,0),false);
        addSeries(series_detect_rate_3D_, METHOD_LINE_INTER,false,QColor(204,51,0),false);

        emit setLegendVisibility(false);
    }

    current_analysis_ = data;

    axis_y_->setMin(0.0);

    emit chartDataCreated();
}

void Qt_Tools::Chart::toggleSeries(const int& series_code, const bool& enabled){

    auto toggle = [](QAbstractSeries* series, bool enabled){
        if(series != nullptr){
            if(enabled){
                series->show();
            }
            else{
                series->hide();
            }
        }
    };

    auto toggleBox = [this](QBoxPlotSeries* series, bool enabled){
        if(series != nullptr){
            if(enabled){
                addSeries(series,true);
            }
            else{
                chart_->removeSeries(series);
            }
        }
    };

    switch (series_code){

    case SERIES_BOXPLOT_UV:
        toggleBox(series_boxplot_UV_,enabled);
        break;
    case SERIES_DETECTRATE_UV:
        toggle(series_detect_rate_UV_,enabled);
        break;
    case SERIES_MEANMAX_UV:
        for(QScatterSeries* series : series_UV_){
            toggle(series,enabled);
        }
        break;
    case SERIES_MEANVAR_UV:
        toggleBox(series_mean_var_UV_,enabled);
        break;


    case SERIES_BOXPLOT_3D:
        toggleBox(series_boxplot_3D_,enabled);
        break;
    case SERIES_DETECTRATE_3D:
        toggle(series_detect_rate_3D_,enabled);
        break;
    case SERIES_MEANMAX_3D:
        for(QScatterSeries* series : series_3D_){
            toggle(series,enabled);
        }
        break;
    case SERIES_MEANVAR_3D:
        toggleBox(series_mean_var_3D_,enabled);
        break;

    case SERIES_INTER_METHOD:
        toggle(series_method_compare_,enabled);
        break;
    case SERIES_INTER_METHOD_MEAN:
        toggle(series_method_compare_mean_,enabled);
        break;

    default:
        break;

    }

    if(series_detect_rate_3D_->isVisible() || series_detect_rate_UV_->isVisible()){
        axis_y_2_->show();
    }
    else{
        axis_y_2_->hide();
    }
}

void Qt_Tools::Chart::mouseMoveEvent(QMouseEvent* event){
    info_overlay_->hide();

    //find set idx corresponding to current mouse position
    QPointF chart_position = mouseToChart(event->pos());
    chart_position.setY(chart_position.y()/axis_y_->max()*axis_y_2_->max());
    int idx = int(chart_position.x()+0.5);
    if(idx < 0 || (idx > int(axis_x_->max()+0.1)-1) || series_detect_rate_UV_ == nullptr){
        return;
    }

    //check if mouse is located on one of the series points for the determined set
    QPointF best;
    float best_dist = 10000.0;
    if(idx < series_detect_rate_UV_->count()){
        QPointF p = series_detect_rate_UV_->at(idx);
        QPoint global_point = chartToMouse(p,series_detect_rate_UV_);

        float dist = (global_point-event->pos()).manhattanLength();
        if(dist < best_dist){
            best_dist = dist;
            best = series_detect_rate_UV_->at(idx);
        }
    }

    if(best_dist > 10){
        return;
    }

    //show set name and value in overlay
    if(current_analysis_.type == ANALYSIS_TYPE_BOTH){
        idx = int(float(idx)/1.2);
    }
    info_overlay_->setText(current_analysis_.set_name[idx]+"\n"+QString::number(best.y()));
    info_overlay_->setGeometry(event->pos().x()+20,event->pos().y()+20,info_overlay_->width(),info_overlay_->height());
    info_overlay_->adjustSize();
    info_overlay_->show();
}

void Qt_Tools::Chart::wheelEvent(QWheelEvent* event){
    QPointF chart_position = mouseToChart(event->pos());
    float range = axis_x_->max()-axis_x_->min();

    float scale = float(1000+event->delta())/1000.0;
    if(abs(chart_position.x()) < range/10.0){
        axis_y_->setMax(scale*axis_y_->max());
    }
    else if(abs(chart_position.x()-axis_x_->max()) < range/10.0){
        axis_y_2_->setMax(scale*axis_y_2_->max());
    }
}

void Qt_Tools::Chart::setFontSize(const int& size){
    QFont font("Times New Roman");
    font.setPointSize(int(size));
    //axis_x_cat_->setTitleBrush(QBrush(QColor(0,0,0)));
    axis_x_cat_->setTitleFont(font);
    axis_x_cat_->setLabelsFont(font);
    axis_x_cat_->setLabelsBrush(QBrush(QColor(0,0,0)));
    axis_y_->setTitleFont(font);
    axis_y_->setLabelsFont(font);
    axis_y_2_->setTitleFont(font);
    axis_y_2_->setLabelsFont(font);
}

void Qt_Tools::Chart::setPenWidth(const int& size){
    QPen pen = axis_x_cat_->linePen();
    pen.setWidth(size);
    axis_x_cat_->setLinePen(pen);
    axis_y_->setLinePen(pen);
    axis_y_2_->setLinePen(pen);

    line_width_ = size;
    showAnalysis(current_analysis_);
}
